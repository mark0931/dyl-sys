package dyl.common.bean;

import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;

public class UrlLog {
	private String url;
	private String Para;
	private String loginUser;
	private String ip;
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date time;
	public UrlLog(){
		
	}
	
	public UrlLog(String url, String para, String loginUser, String ip,Date time){
		super();
		this.url = url;
		Para = para;
		this.loginUser = loginUser;
		this.ip = ip;
		this.time = time;
	}

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getPara() {
		return Para;
	}
	public void setPara(String para) {
		Para = para;
	}
	public String getLoginUser() {
		return loginUser;
	}
	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	
	
}
